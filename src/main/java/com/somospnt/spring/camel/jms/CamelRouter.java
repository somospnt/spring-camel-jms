package com.somospnt.spring.camel.jms;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class CamelRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("activemq:foo")
                .unmarshal().jacksonxml(Mensaje.class)
                .to("log:sample");

        from("timer:bar")
                .setBody(constant("<mensaje> <texto>Hola!!!</texto> </mensaje>"))
                .to("activemq:foo");
    }
}
